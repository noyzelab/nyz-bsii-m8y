# NYZ BSII M8Y

Pure Data patch to assist with editing the Novation Bass Station II consisting of 3 main editors. Controllable via the mouse & computer keyboard, as well as with a MIDI controller/keyboard capable of sending NRPN & CC messages.  

**i] AFX Mode**

**ii] Microtuning**

**iii] On-key Functions**



NOTE: this is not meant to be an editor for all BSII parameters, so there is no replication of panel functions [pots/switches etc]. the export functions in AFX Mode will require an external MIDI capture program, such as Sysex Librarian or MIDI OX. you can use the computer keyboard for some actions [**BUT THESE ARE ANYTHING BUT FINAL..** and could well change during the final dev stages so dont get too attached:] .. for stuck notes hit the 'a' key for midi all notes off command. each editor can also be driven remotely by your master keyboard / midi controller. below is the current list of ascii computer keypresses and nrpn/cc messages the patch will respond to. nrpn and cc is listed as **1:nn/cc** where **nn** is the nrpn LSB number and **cc** is the continuous controller number  =>


**ii] AFX mode**

select overlay bank - 1 to 8, 0 = no overlay

p - write protect overlay

r - export all overlay

y - clear single overlay

t - export single overlay

s - auto note stepper

d - manual note stepper

1:10/11 selek overlay bank

1:11/12 write protect

1:12/13 export all 25 keys

1:13/14 clear overlay bank

1:14/15 store overlay bank

1:15/16 copy key

1:16/17 move key

1:17/18 export key

1:18/19 clear key

**only have one of these active at a time, its for midi note input of overlay key selections =>**

1:19/20 copy/move - source key input active 

1:20/21 copy/move - destination key input active 

1:21/22 export - key input active 

1:22/23 clear - key input active 


**ii] Microtuning [note: this is standard MTS per key, so you can use this section on ANY synth that supports this standard, but u will need to save on the synth, the save button is a unique sysex command to BSII**

select microtuning table - z to for tables 1 to 8, and . = table 0

1:0/3 select tuning table

1:1/4 mt crs

1:2/8 mt fin

1:3/9 mt edit

1:4/10 store2tab [please note its possible to alter table 0, giving effectively 9 user tuning tables, but you cannot store table 0. Novation have this table pre-programmed to 12tet at boot up]


**iii] On-Key Functions** - all sliders will transmit nrpn or cc as allocated by novation. my alternative cc in's for the novation nrpn commands are listed after these values. e.g. for osc error 0:111 is the nrpn allocated by novation, and cc30 is the alternative i have allocated


0:111/30 - osc error

cc110  sync1-2

0:107/31 paraphonic

0:84/35 sub osc crs

0:77/40 sub osc fin

cc107 pitch bend range

64:6/41 master tune [note the bsii does not respond to this correctly, i expect this will be fixed in a firmware update]

0:113/42 glide diverge 

0:70/43 mw to lfo1 to osc pitch

0:78/44 mw to osc2 pitch

0:75/45 at to lfo1 to osc pitch

cc116 arp swing

0:106/46 seq retrig

64:28/47 input gain

0:108/48 Fc tracking

0:94/49 mw to Fc

0:71/50 MW ->LFO2 -> Fc

0:74/51 AT -> Fc

cc113 mod env vel

0:118/52 mod env num cycles

0:110/53 mod env retrig

0:115/54 mod env fixed dur

cc112 amp env vel

0:117/55 amp env num cycles

0:109/56 amp env retrig

0:114/57 amp env fixed dur

cc95 amp limiter

0:86/58 lfo1 slew

0:89/59 lfo1 key sync

0:88/60 lfo1 speed/sync

0:90/61 lfo2 slew

0:93/62 lfo2 key sync

0:92/63 lfo2 speed/sync

0:76/65 AT -> LFO2 speed


**TODO**

tidying display, add additional/modify existing ascii keypresses, reassign any nrpn/cc if necessary, making the readouts match the range for certain parameters rather than display the raw midi output, tidy final patch for readability

**thanks =>**

beta testing & special advisory natteringz: Richard D James

the folks at Novation: Jerome Meunier, Matt Speed, Mario Buoninfante for encouragement!


**how to keep me going ==>**

if u find this prog useful please think about supporting my work either thru my bandcamp page:
https://noyzelab.bandcamp.com/
or get in touch via noyzelab [at] gmail [dot] com
thanks, dave